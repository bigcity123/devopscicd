<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;


class ValidatePasswordTest extends TestCase {

    public function testValidLength(){
        $valPass =new ValidatePassword();
        $this->assertFalse($valPass->validLength('1234'));
    }
}